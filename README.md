# Weather Service
A simple weather microservice which demonstrates backend integration and testing with OpenWeatherMap API ([https://openweathermap.org]( https://openweathermap.org))

## Prerequisite
The following tech-stack is necessary for application development and testing:
 
- Java JDK 11, Spring-boot
- Maven 
- Docker 

## How to run & test
To run unit and integration test
```bash 
mvn clean verify
```
To run the locally 
```bash
mvn spring-boot:run
```

To run with docker
```bash 
mvn clean install
docker-compose up
```

### Current weather api `GET /api/weather/current?location={city}`
This API takes in a location parameter (with format like `Berlin` or `Berlin,de`) and returns:
- the current temperature in degrees celsius
- the current air pressure in `hPa`
- a boolean whether to take an umbrella (`true`) or not (`false`)

```
❯ curl 'http://localhost:8080/api/weather/current?location=Berlin,de' | json_pp  
{
   "pressure" : 1020,
   "temp" : 4.72,
   "umbrella" : false
}
```

Malformed input handling: 
```
❯ curl 'http://localhost:8080/api/weather/history?location=' | json_pp  
{
   "status" : 400,
   "title" : "Location input cannot be empty, please enter either 'city' or 'city,country' format",
   "type" : "error.validation"
}

```

### Historical weather api `GET /api/weather/history?location={city}`
This api also take in the current city name as above, and returns the historical data as well as an average over the last 5 queries for the same city

```
❯ curl 'http://localhost:8080/api/weather/history?location=Berlin,de' | json_pp 
{
   "avg_pressure" : 1020,
   "avg_temp" : 4.72,
   "history" : [
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      },
      {
         "pressure" : 1020,
         "temp" : 4.72,
         "umbrella" : false
      }
   ]
}


```
## How the production service should look like
In order to make this microservice production ready, besides improving on test coverage and performance optimization, there are some more steps I believe necessary: 

- Secure the api endpoints with Spring security, turn on `csrf` and headers policies, also apply authentication like using `jwt` token or similar methods to ensure authorized API consumption.
- Fully configured production ready database, like `postgresql` or `rds` 
- Add monitoring tool like `prometheus` and `grafana` to view health and performance metrics
- Add service discovery like `Consul` or `Eureka` or `Zookeeper`, so that other microservice can discover this microservice better.
- Add event sourcing capability, like using `Kafka` or `RabbitMQ` to announce each time a new weather data is recorded, this way other microservices don't need to fully rely on this microservice endpoints or database to get the latest updates. 
- Add `checkstyle` and `sonarq` for improving code commit and quality
- Add API tool like `swagger` to generate API documentation automatically, to help with development productivity
- Use AspectJ AOP for logging to have standard logs on exception or method enter/exit.
- Apply AuditingEvent when data is inserted or modified
- Apply data migration tool like `liquibase` to automate the process of creating, updating data schema when this microservice grows.
- Improve REST schema, probably a more standardize with other CRUD capability and follow the spring HATEOAS standard [https://spring.io/projects/spring-hateoas](https://spring.io/projects/spring-hateoas)
- ...(and this list can keep growing, especially some more specific settings for AWS need to be applied as well)

## How to deploy
The application was configured with `terraform` for `aws` environment, please see `terraform` folder as example/template and adjust according to your platform setting.
```
# terraform/ecs.tf
variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "035898547283.dkr.ecr.us-west-2.amazonaws.com/weather-service_app:latest"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 8080
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 3
}
```
Deploy command: 
```bash
terraform init terraform/
terraform apply terraform/
```

## Things to improve 
What a great challenge to work on! After nearly 5 hours I still see there are many things to improve, here are some of them:
 
- API decoupling:  
The current `GET /api/weather/current` is totally coupled with open weather api, it means that when `api.openweather.org` is down, this endpoint will not work as expected.
To improve this coupling we could introduce a [circuit breaker](https://cloud.spring.io/spring-cloud-netflix/multi/multi__circuit_breaker_hystrix_clients.html) using tool like `hystrix`, so in case the external service is unreachable, the endpoint can return latest weather data from local database.

- Better input validation:
Currently city and country are accepted from user input via parameters without a predefined set of truth, the microservice can have a local store of city and country in order to validate user input better.

- Paging, indexing, searching capability: 
Currently the list of history is returned without paging, this could cause performance issue and can be improved by applying/agreeing on paging. 
Also the city and country should be marked for indexed because these fields are used for searching for weather records.
It's also good to add fields like date/time, and track request for further analytics.
   
