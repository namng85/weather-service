FROM adoptopenjdk/openjdk11:ubi
MAINTAINER namnvhue@gmail.com

EXPOSE 8080
EXPOSE 3306
VOLUME /tmp
ADD target/weather-service-0.0.1-SNAPSHOT.jar app.jar
CMD ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]