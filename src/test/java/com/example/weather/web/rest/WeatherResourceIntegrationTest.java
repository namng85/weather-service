package com.example.weather.web.rest;

import com.example.weather.client.dto.OpenWeatherDto;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.junit.jupiter.MockServerExtension;
import org.mockserver.junit.jupiter.MockServerSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.example.utils.TestUtils.toJsonString;
import static java.lang.String.format;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles({"test"})
@ExtendWith({MockServerExtension.class, SpringExtension.class})
@MockServerSettings(ports = {8128})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WeatherResourceIntegrationTest {
    @Autowired
    private WebApplicationContext wac;
    private ClientAndServer mockServer;

    private MockMvc mockMvc;

    @BeforeAll
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @BeforeEach
    public void setupEach(ClientAndServer mockServer) {
        this.mockServer = mockServer;
    }


    @Test
    @Sql(scripts = "/sql/clean-up.sql", executionPhase = AFTER_TEST_METHOD)
    public void shouldGetCurrentWeather() throws Exception {
        // GIVEN
        var location = "Berlin,de";
        var openWeatherResult = OpenWeatherDto.builder()
                .main(OpenWeatherDto.Main.builder()
                        .temp(7.0)
                        .pressure(1234)
                        .build())
                .weather(List.of(OpenWeatherDto.Weather.builder()
                        .main("Rain")
                        .build()))
                .build();

        mockServer.when(
                request()
                        .withMethod(GET.name())
                        .withQueryStringParameter("q", location)
                        .withPath("/weather"))
                .respond(response()
                        .withBody(toJsonString(openWeatherResult)));

        // WHEN
        mockMvc.perform(get(format("/api/weather/current?location=%s", location)))
                .andDo(print())
        // THEN
                .andExpect(jsonPath("$.temp").value(7.0))
                .andExpect(jsonPath("$.pressure").value(1234))
                .andExpect(jsonPath("$.umbrella").value(true));
    }

    @Test
    @Sql(scripts = "/sql/clean-up.sql", executionPhase = AFTER_TEST_METHOD)
    public void shouldReturnException() throws Exception {
        // GIVEN
        String location = "";

        // WHEN
        mockMvc.perform(get(format("/api/weather/current?location=%s", location)))
                .andDo(print())
        // THEN
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.type").value("error.validation"));
    }

    @Test
    @Sql(scripts = {"/sql/weather-data.sql"})
    @Sql(scripts = "/sql/clean-up.sql", executionPhase = AFTER_TEST_METHOD)
    public void shouldReturnHistory() throws Exception {
        // GIVEN
        var location = "Berlin,de";

        // WHEN
        mockMvc.perform(get(format("/api/weather/history?location=%s", location)))
                .andDo(print())
        // THEN
                .andExpect(jsonPath("$.avg_temp").value(7.0))
                .andExpect(jsonPath("$.avg_pressure").value(1250.0))
                .andExpect(jsonPath("$.history[0].temp").value(8.0))
                .andExpect(jsonPath("$.history[0].pressure").value(1500))
                .andExpect(jsonPath("$.history[1].temp").value(6.0))
                .andExpect(jsonPath("$.history[1].pressure").value(1000));
    }
}