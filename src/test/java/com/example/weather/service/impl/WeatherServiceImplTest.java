package com.example.weather.service.impl;

import com.example.weather.client.OpenWeatherGateway;
import com.example.weather.client.dto.OpenWeatherDto;
import com.example.weather.domain.CityWeather;
import com.example.weather.exception.OpenWeatherServiceException;
import com.example.weather.repository.CityWeatherRepository;
import com.example.weather.web.converter.LocationInputConverter;
import com.example.weather.web.dto.WeatherDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class WeatherServiceImplTest {
    @Mock
    private CityWeatherRepository cityWeatherRepository;

    @Mock
    private OpenWeatherGateway openWeatherGateway;

    @InjectMocks
    private WeatherServiceImpl weatherService;

    @Captor
    private ArgumentCaptor<CityWeather> cityWeatherArgumentCaptor;

    @BeforeEach
    private void setup() {
        ReflectionTestUtils.setField(weatherService, "umbrellaCondition", "Thunderstorm,Drizzle,Rain");
    }

    @Test
    public void shouldGetCurrentWeather() {
        // GIVEN
        var location = "Berlin,de";
        var locationDto = LocationInputConverter.convertUserInput(location);
        var openWeatherResult = OpenWeatherDto.builder()
                .main(OpenWeatherDto.Main.builder()
                        .temp(7.0)
                        .pressure(1234)
                        .build())
                .weather(List.of(OpenWeatherDto.Weather.builder()
                        .main("Rain")
                        .build()))
                .build();
        given(openWeatherGateway.getWeather(location)).willReturn(openWeatherResult);

        // WHEN
        var currentWeather = weatherService.getCurrentWeather(locationDto);

        // THEN
        then(cityWeatherRepository).should(times(1)).save(cityWeatherArgumentCaptor.capture());
        var entity = cityWeatherArgumentCaptor.getValue();

        assertThat(entity.getCity(), is("Berlin"));
        assertThat(entity.getCountry(), is("de"));
        assertThat(entity.getTemp(), is(7.0));
        assertThat(entity.getPressure(), is(1234));
        assertThat(entity.getUmbrella(), is(true));

        assertThat(currentWeather.getTemp(), is(7.0));
        assertThat(currentWeather.getPressure(), is(1234));
        assertThat(currentWeather.getUmbrella(), is(true));
    }

    @Test
    public void shouldThrowException() {
        // GIVEN
        var location = "Berlin,de";
        var locationDto = LocationInputConverter.convertUserInput(location);
        var openWeatherResult = OpenWeatherDto.builder()
                .main(OpenWeatherDto.Main.builder()
                        .temp(7.0)
                        .pressure(1234)
                        .build())
                .weather(List.of())
                .build();
        given(openWeatherGateway.getWeather(location)).willReturn(openWeatherResult);

        // WHEN
        assertThrows(OpenWeatherServiceException.class, () -> weatherService.getCurrentWeather(locationDto));

        // THEN
        then(cityWeatherRepository).should(never()).save(cityWeatherArgumentCaptor.capture());
    }

    @Test
    public void shouldCalculateAvg() {
        // GIVEN
        var location = "Berlin,de";
        var locationDto = LocationInputConverter.convertUserInput(location);
        var weather1 = CityWeather.builder()
                .city("Berlin")
                .country("de")
                .temp(6.0)
                .pressure(1000)
                .umbrella(true)
                .build();

        var weather2 = CityWeather.builder()
                .city("Berlin")
                .country("de")
                .temp(8.0)
                .pressure(1500)
                .umbrella(true)
                .build();

        given(cityWeatherRepository.findAllByCityAndCountryOrderByIdDesc("Berlin", "de")).willReturn(List.of(weather1, weather2));

        // WHEN
        var historyWeather = weatherService.getHistoryWeather(locationDto);

        // THEN
        assertThat(historyWeather.getAvgTemp(), is(7.0));
        assertThat(historyWeather.getAvgPressure(), is(1250.0));
        assertThat(historyWeather.getHistory(), containsInAnyOrder(
                WeatherDto.from(weather1),
                WeatherDto.from(weather2)
        ));
    }
}