package com.example.weather.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.form.FormEncoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.example.weather.client")
public class FeignConfig {

    private static final ObjectMapper OBJECT_MAPPER = CustomObjectMapper.create();

    @Bean
    public Encoder feignEncoder() {
        return new FormEncoder(new JacksonEncoder(OBJECT_MAPPER));
    }

    @Bean
    public Decoder decoder() {
        Decoder decoder = (response, type) -> new JacksonDecoder(OBJECT_MAPPER).decode(response, type);
        return new ResponseEntityDecoder(decoder);
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.BASIC;
    }

}