package com.example.weather.client;

import com.example.weather.client.dto.OpenWeatherDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OpenWeatherGateway {
    private final OpenWeatherClient openWeatherClient;
    @Value("${remote.clients.openweather.apiKey}")
    private String apiKey;
    @Value("${remote.clients.openweather.units}")
    private String units;

    public OpenWeatherGateway(OpenWeatherClient openWeatherClient) {
        this.openWeatherClient = openWeatherClient;
    }

    public OpenWeatherDto getWeather(String city) {
        return openWeatherClient.getWeather(city, apiKey, units);
    }
}
