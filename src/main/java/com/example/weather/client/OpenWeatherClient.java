package com.example.weather.client;

import com.example.weather.client.dto.OpenWeatherDto;
import com.example.weather.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "openweather", url = "${remote.clients.openweather.url}", configuration = FeignConfig.class)
public interface OpenWeatherClient {
    @GetMapping(value = "/weather?q={city}&APPID={apiKey}&units={units}", produces = MediaType.APPLICATION_JSON_VALUE)
    OpenWeatherDto getWeather(@PathVariable String city, @PathVariable String apiKey, @PathVariable String units);
}
