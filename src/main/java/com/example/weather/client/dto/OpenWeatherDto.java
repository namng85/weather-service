package com.example.weather.client.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OpenWeatherDto {
    private List<Weather> weather;
    private Main main;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static final class Weather {
        private Integer id;
        private String main;
        private String description;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static final class Main {
        private Double temp;
        private Integer pressure;
    }
}
