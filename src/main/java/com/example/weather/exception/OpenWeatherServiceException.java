package com.example.weather.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class OpenWeatherServiceException extends RuntimeException {
    public OpenWeatherServiceException(String message) {
        super(message);
    }
}
