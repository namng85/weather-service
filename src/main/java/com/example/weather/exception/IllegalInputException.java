package com.example.weather.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class IllegalInputException extends RuntimeException {
    public IllegalInputException(String message) {
        super(message);
    }

    public static IllegalInputException withLocation(String location) {
        return new IllegalInputException(String.format("User input location('%s') is not valid, please enter either 'city' or 'city,country' format", location));
    }
}



