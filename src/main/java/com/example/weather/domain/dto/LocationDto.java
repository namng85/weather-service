package com.example.weather.domain.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
@Builder
public class LocationDto {
    private String city;
    private String country;

    public String buildQueryString() {
        if (StringUtils.hasLength(country)) {
            return getCity() + "," + getCountry();
        } else {
            return getCity();
        }
    }
}
