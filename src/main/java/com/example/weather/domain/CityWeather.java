package com.example.weather.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "city_weather")
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CityWeather implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private String country;

    @Column(name = "temp")
    private Double temp;

    @Column(name = "pressure")
    private Integer pressure;

    @Column(name = "umbrella")
    private Boolean umbrella;

}
