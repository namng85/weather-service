package com.example.weather.repository;

import com.example.weather.domain.CityWeather;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the CityWeather entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityWeatherRepository extends JpaRepository<CityWeather, Long> {
    List<CityWeather> findAllByCityOrderByIdDesc(String city);

    List<CityWeather> findAllByCityAndCountryOrderByIdDesc(String city, String country);
}
