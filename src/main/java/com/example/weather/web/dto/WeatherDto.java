package com.example.weather.web.dto;

import com.example.weather.domain.CityWeather;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherDto {
    private Double temp;
    private Integer pressure;
    private Boolean umbrella;

    public static WeatherDto from(CityWeather cityWeather) {
        return WeatherDto.builder()
                .pressure(cityWeather.getPressure())
                .temp(cityWeather.getTemp())
                .umbrella(cityWeather.getUmbrella())
                .build();
    }
}
