package com.example.weather.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class WeatherHistoryDto {
    @JsonProperty("avg_temp")
    private Double avgTemp;

    @JsonProperty("avg_pressure")
    private Double avgPressure;

    private List<WeatherDto> history;
}
