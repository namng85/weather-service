package com.example.weather.web.converter;

import com.example.weather.domain.dto.LocationDto;
import com.example.weather.exception.IllegalInputException;
import org.springframework.util.StringUtils;

public class LocationInputConverter {
    public static LocationDto convertUserInput(String location) {
        if (!StringUtils.hasLength(location)) {
            throw new IllegalInputException("Location input cannot be empty, please enter either 'city' or 'city,country' format");
        }
        String[] split = location.split(",");
        if (split.length > 2) {
            throw IllegalInputException.withLocation(location);
        }
        String country = null;
        if (split.length > 1) {
            country = split[1];
        }
        return LocationDto.builder()
                .city(split[0])
                .country(country)
                .build();
    }
}
