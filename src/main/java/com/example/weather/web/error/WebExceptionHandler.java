package com.example.weather.web.error;

import com.example.weather.exception.IllegalInputException;
import com.example.weather.exception.OpenWeatherServiceException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

import java.net.URI;

@ControllerAdvice
public class WebExceptionHandler {
    public static final String ERR_VALIDATION = "error.validation";
    public static final String OPEN_WEATHER_UNAVAILABLE = "error.open-weather";

    @ExceptionHandler(IllegalInputException.class)
    public ResponseEntity<Problem> handleInput(IllegalInputException exception) {
        Problem problem = Problem.builder()
                .withStatus(Status.BAD_REQUEST)
                .withType(URI.create(ERR_VALIDATION))
                .withTitle(exception.getMessage())
                .build();

        return ResponseEntity.badRequest().body(problem);
    }

    @ExceptionHandler(OpenWeatherServiceException.class)
    public ResponseEntity<Problem> handleOpenWeatherException(OpenWeatherServiceException exception) {
        Problem problem = Problem.builder()
                .withStatus(Status.SERVICE_UNAVAILABLE)
                .withType(URI.create(OPEN_WEATHER_UNAVAILABLE))
                .withTitle(exception.getMessage())
                .build();

        return ResponseEntity.badRequest().body(problem);
    }
}
