package com.example.weather.web.rest;

import com.example.weather.service.WeatherService;
import com.example.weather.web.converter.LocationInputConverter;
import com.example.weather.web.dto.WeatherDto;
import com.example.weather.web.dto.WeatherHistoryDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@RequestMapping("/api/weather")
@Slf4j
public class WeatherResource {
    private final WeatherService weatherService;

    @GetMapping("/current")
    public ResponseEntity<WeatherDto> getCurrent(@RequestParam String location) {
        log.info("REST request to get current weather for {}", location);
        WeatherDto currentWeather = weatherService.getCurrentWeather(LocationInputConverter.convertUserInput(location));

        return ResponseEntity.ok(currentWeather);
    }

    @GetMapping("/history")
    public ResponseEntity<WeatherHistoryDto> getHistory(@RequestParam String location) {
        log.info("REST request to get history weather for {}", location);
        WeatherHistoryDto historyWeather = weatherService.getHistoryWeather(LocationInputConverter.convertUserInput(location));

        return ResponseEntity.ok(historyWeather);
    }
}
