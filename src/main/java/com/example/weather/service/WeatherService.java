package com.example.weather.service;

import com.example.weather.domain.dto.LocationDto;
import com.example.weather.web.dto.WeatherDto;
import com.example.weather.web.dto.WeatherHistoryDto;

public interface WeatherService {
    WeatherDto getCurrentWeather(LocationDto location);

    WeatherHistoryDto getHistoryWeather(LocationDto location);
}
