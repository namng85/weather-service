package com.example.weather.service.impl;

import com.example.weather.client.OpenWeatherGateway;
import com.example.weather.client.dto.OpenWeatherDto;
import com.example.weather.domain.CityWeather;
import com.example.weather.domain.dto.LocationDto;
import com.example.weather.exception.OpenWeatherServiceException;
import com.example.weather.repository.CityWeatherRepository;
import com.example.weather.service.WeatherService;
import com.example.weather.web.dto.WeatherDto;
import com.example.weather.web.dto.WeatherHistoryDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class WeatherServiceImpl implements WeatherService {
    private final OpenWeatherGateway openWeatherGateway;
    private final CityWeatherRepository cityWeatherRepository;
    private static final Double ZERO = 0.0;
    @Value("${weather.condition.umbrella}")
    private String umbrellaCondition;

    public WeatherServiceImpl(OpenWeatherGateway openWeatherGateway, CityWeatherRepository cityWeatherRepository) {
        this.openWeatherGateway = openWeatherGateway;
        this.cityWeatherRepository = cityWeatherRepository;
    }

    @Override
    public WeatherDto getCurrentWeather(LocationDto location) {
        log.info("get current weather for location={}", location);
        OpenWeatherDto weather = openWeatherGateway.getWeather(location.buildQueryString());

        if (null == weather.getWeather() || CollectionUtils.isEmpty(weather.getWeather()) ||
                !StringUtils.hasLength(weather.getWeather().get(0).getMain())) {
            log.warn("Open weather service data not available {}", weather);
            throw new OpenWeatherServiceException("Open weather service not return correct data");
        }

        Boolean needUmbrella = Arrays.asList(umbrellaCondition.split(","))
                .contains(weather.getWeather().get(0).getMain());

        CityWeather entity = CityWeather.builder()
                .city(location.getCity())
                .country(location.getCountry())
                .pressure(weather.getMain().getPressure())
                .temp(weather.getMain().getTemp())
                .umbrella(needUmbrella)
                .build();

        cityWeatherRepository.save(entity);

        log.info("Saved weather data: {}", entity);
        return WeatherDto.from(entity);
    }

    @Override
    public WeatherHistoryDto getHistoryWeather(LocationDto location) {
        log.info("Get weather history for: {}", location);
        List<CityWeather> weatherList;
        if (StringUtils.hasLength(location.getCountry())) {
            weatherList = cityWeatherRepository.findAllByCityAndCountryOrderByIdDesc(location.getCity(), location.getCountry());
        } else {
            weatherList = cityWeatherRepository.findAllByCityOrderByIdDesc(location.getCity());
        }

        Double avgTemp = weatherList.stream()
                .limit(5)
                .mapToDouble(CityWeather::getTemp)
                .average()
                .orElse(ZERO);

        Double avgPressure = weatherList.stream()
                .limit(5)
                .mapToDouble(CityWeather::getPressure)
                .average()
                .orElse(ZERO);

        List<WeatherDto> history = weatherList.stream().map(WeatherDto::from).collect(Collectors.toList());

        log.info("Weather history = avgTemp: {}, avgPressure: {}, history: {}", avgTemp, avgPressure, history);
        return WeatherHistoryDto.builder()
                .avgTemp(avgTemp)
                .avgPressure(avgPressure)
                .history(history)
                .build();
    }
}
